package br.com.trama.androidcustomcomponents;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import br.com.trama.androidcustomcomponents.datetimepicker.DateTimePickerFragment;
import br.com.trama.androidcustomcomponents.demo.R;

public class DemoActivity extends FragmentActivity{

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

	public void showDateTimePickerDialog(View v) {
	    DialogFragment newFragment = new DateTimePickerFragment();
	    newFragment.show(getSupportFragmentManager(), "DateTimePickerFragment");
	}
}

