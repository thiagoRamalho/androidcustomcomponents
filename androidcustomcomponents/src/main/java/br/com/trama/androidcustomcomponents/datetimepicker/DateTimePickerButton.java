package br.com.trama.androidcustomcomponents.datetimepicker;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import br.com.trama.androidcustomcomponents.demo.R;

public class DateTimePickerButton extends LinearLayout implements DateTimePickerDialog.OnDateTimeSetListener{

	private EditText editText;
	private Calendar calendar;
    private final java.text.DateFormat dateTimeInstance;

	public DateTimePickerButton(Context context) {
		this(context, null);
	}
    
	public DateTimePickerButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		
		setOrientation(LinearLayout.HORIZONTAL);
	    setGravity(Gravity.CENTER_VERTICAL);

	    LayoutInflater inflater = (LayoutInflater) context
	        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	   
	    View view = inflater.inflate(R.layout.date_time_picker_button, this, true);
	    
	    editText = (EditText) view.findViewById(R.id.editTextDtPickerButton);
	    ImageButton imageButton = (ImageButton) view.findViewById(R.id.imageButtonDatePickerButton);
	    calendar = Calendar.getInstance();
	    
        dateTimeInstance = java.text.DateFormat.getDateTimeInstance();
	    
		editText.setText(""+this.dateTimeInstance.format(calendar.getTime()));
		Click click = new Click();
		
		editText.setOnClickListener(click);
		imageButton.setOnClickListener(click);
	}
	
	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
		editText.setText(this.dateTimeInstance.format(calendar));
	}

	public void onDateTimeSet(int year, int monthOfYear, int dayOfMonth,
			
			int currentHour, int currentMinutes) {
		
		calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth, currentHour, currentMinutes, 0);
		
		editText.setText(this.dateTimeInstance.format(calendar.getTime()));
	}
	
	public Calendar getCalendar(){
		return calendar;
	}
	
	class Click implements OnClickListener{

		DateTimePickerDialog newFragment; 
		
		public Click() {
			
			if(!DateTimePickerButton.this.isInEditMode()){
			newFragment = new DateTimePickerDialog(DateTimePickerButton.this.getContext(), DateTimePickerButton.this, 
					DateTimePickerButton.this.calendar);
			}
		}
		
		public void onClick(View v) {
			newFragment.show();
		}
	}
}
