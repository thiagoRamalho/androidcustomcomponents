package br.com.trama.androidcustomcomponents.datetimepicker;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;
import br.com.trama.androidcustomcomponents.demo.R;

/**
 * A simple dialog containing an {@link android.widget.DatePicker} and {@link android.widget.TimePicker}.
 * Based on DatePickerDialog
 */
@SuppressLint("InflateParams")
public class DateTimePickerDialog extends AlertDialog 
implements OnClickListener, OnDateChangedListener, OnTimeChangedListener {

    private static final String YEAR  = "year";
    private static final String MONTH = "month";
    private static final String DAY   = "day";
    private static final String HOUR  = "hour";
    private static final String MIN   = "min";
    
    private final DatePicker mDatePicker;
    private final TimePicker mTimePicker;
    private final OnDateTimeSetListener mCallBack;
    private final java.text.DateFormat mTitleDateFormat;

    /**
     * The callback used to indicate the user is done filling in the date.
     */
    public interface OnDateTimeSetListener {
        void onDateTimeSet(int year, int monthOfYear, int dayOfMonth, int currentHour, int currentMinutes);
    }

    public DateTimePickerDialog(Context context,
    		OnDateTimeSetListener callBack) {
        this(context, callBack, Calendar.getInstance());
    }

    
    public DateTimePickerDialog(Context context, OnDateTimeSetListener callBack, Calendar calendar) {
		
    	super(context);
    	
        mCallBack = callBack;
		
        mTitleDateFormat = java.text.DateFormat.getDateInstance(java.text.DateFormat.FULL);
        
        updateTitle(calendar);
        
        LayoutInflater inflater = 
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.date_time_layout, null);
        
        setView(view);
        
        setButton(BUTTON_NEUTRAL, context.getText(android.R.string.ok), this);
        
        int day    = calendar.get(Calendar.DAY_OF_MONTH);
        int month  = calendar.get(Calendar.MONTH);
        int year   = calendar.get(Calendar.YEAR);
        int hour   = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        
        mDatePicker = (DatePicker) view.findViewById(R.id.datePickerComponent);
        mDatePicker.init(year, month, day, this);
        
        mTimePicker = (TimePicker) view.findViewById(R.id.timePickerComponent);
        mTimePicker.setIs24HourView(true);
        mTimePicker.setCurrentHour(hour);
        mTimePicker.setCurrentMinute(minute);
        mTimePicker.setOnTimeChangedListener(this);
	}
    
    public void onClick(DialogInterface dialog, int which) {
    	if (mCallBack != null) {
    		mDatePicker.clearFocus();
    		mCallBack.onDateTimeSet(
    				mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth(), 
    				mTimePicker.getCurrentHour(), mTimePicker.getCurrentMinute());
    	}
    }
    
    public void onDateChanged(DatePicker view, int year, int month, int day) {
        updateTitle(new GregorianCalendar(year, month, day, mTimePicker.getCurrentHour(), mTimePicker.getCurrentMinute()));
    }

	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        updateTitle(new GregorianCalendar(mDatePicker.getYear(), mDatePicker.getMonth(), mDatePicker.getDayOfMonth(), hourOfDay, minute));
	}
    
    private void updateTitle(Calendar c) {
    	setTitle(mTitleDateFormat.format(c.getTime()));
    }
    
    @Override
    public Bundle onSaveInstanceState() {
       
    	Bundle state = super.onSaveInstanceState();
       
        state.putInt(YEAR,  mDatePicker.getYear());
        state.putInt(MONTH, mDatePicker.getMonth());
        state.putInt(DAY,   mDatePicker.getDayOfMonth());        
        state.putInt(HOUR,  mTimePicker.getCurrentHour());
        state.putInt(MIN,   mTimePicker.getCurrentMinute());
        
        return state;
    }
    
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        
        int year  = savedInstanceState.getInt(YEAR);
        int month = savedInstanceState.getInt(MONTH);
        int day   = savedInstanceState.getInt(DAY);
        int hour  = savedInstanceState.getInt(HOUR);
        int min   = savedInstanceState.getInt(MIN);
        
        mDatePicker.init(year, month, day, this);
        mTimePicker.setCurrentHour(hour);
        mTimePicker.setCurrentMinute(min);
        
        updateTitle(new GregorianCalendar(year, month, day, hour, min));
    }
}

