package br.com.trama.androidcustomcomponents.datetimepicker;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;
import br.com.trama.androidcustomcomponents.demo.R;


public class DateTimePickerFragment extends DialogFragment implements OnDateChangedListener, OnTimeChangedListener{

	private Calendar actualDateTime;
	private java.text.DateFormat dateTimeInstance;
	private Dialog dialog;
	private View view;
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        
		// Use the current time as the default values for the picker
		actualDateTime = Calendar.getInstance();
        
        int day    = actualDateTime.get(Calendar.DAY_OF_MONTH);
        int month  = actualDateTime.get(Calendar.MONTH);
        int year   = actualDateTime.get(Calendar.YEAR);
        int hour   = actualDateTime.get(Calendar.HOUR_OF_DAY);
        int minute = actualDateTime.get(Calendar.MINUTE);

        dialog = this.create();

        dateTimeInstance = java.text.DateFormat.getDateTimeInstance(java.text.DateFormat.MEDIUM, java.text.DateFormat.SHORT);
        
        setUpdateTitle();
        
        DatePicker datePicker = (DatePicker) view.findViewById(R.id.datePickerComponent);
        datePicker.init(year, month, day, this);
        
        TimePicker timePicker = (TimePicker) view.findViewById(R.id.timePickerComponent);
        timePicker.setIs24HourView(true);
        timePicker.setCurrentHour(hour);
        timePicker.setCurrentMinute(minute);
        timePicker.setOnTimeChangedListener(this);
       
        
        return dialog;
    }
	
	@SuppressLint("InflateParams")
	private Dialog create(){
		
		// Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        
        view = layoutInflater.inflate(R.layout.date_time_layout, null);
        
        builder.setView(view);
        
        builder
               .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                   }
               });
               
        // Create the AlertDialog object and return it
        return builder.create();
	}

	private void setUpdateTitle() {
		dialog.setTitle(dateTimeInstance.format(actualDateTime.getTime()));
	}

	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		
		this.actualDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
		this.actualDateTime.set(Calendar.MINUTE,      minute);
		this.actualDateTime.set(Calendar.SECOND,      0);
		
		this.setUpdateTitle();
	}

	public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
	
		this.actualDateTime.set(Calendar.YEAR,         year);
		this.actualDateTime.set(Calendar.MONTH,        monthOfYear);
		this.actualDateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
		
		this.setUpdateTitle();
	}

}
